FORMAT: 1A
HOST: http://localhost

# ESI14-RentIt
Excerpt of RentIt's API

# Group Purchase Orders
Notes related resources of the **Plants API**

## Purchase Order Management [/rest/pos]
### Retrieve Purchase Orders [GET]
+ Response 200 (application/json)

        [{
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/pos/10001" }
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ]
            },
            "startDate":"2014-11-12",
            "endDate":"2014-11-14",
            "cost":750.0
        },
        {
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/pos/10002" }
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ]
            },
            "startDate":"2014-11-12",
            "endDate":"2014-11-14",
            "cost":750.0
        }
        ]

### Create Purchase Order [POST]
+ Request (application/json)

        {
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ]
            },
            "startDate":"2014-11-12",
            "endDate":"2014-11-14",
        }


+ Response 201 (application/json)

        {
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/pos/10001" }
            ],
            "_links":[
                { "rel":"acceptPO", "href":"http://localhost:9000/rest/pos/10001/accept", "method":"POST" },
                { "rel":"closePO", "href":"http://localhost:9000/rest/pos/10001/accept", "method":"DELETE" }

            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ]
            },
            "startDate":"2014-11-12",
            "endDate":"2014-11-14",
            "cost":"750.0"
        }

+ Response 409 (application/json)

        {
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/pos/10001" }
            ],
            "_links":[
                { "rel":"updatePO", "href":"http://localhost:9000/rest/pos/10001", "method":"PUT" }
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ],
                "_links":[]
            },
            "startDate":"2014-11-12",
            "endDate":"2014-11-14"
        }


## Confirm Purchase Order [/rest/pos/10001/accept]
### Accept a purchase Order[POST]
+ Response 200 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[
                    { "rel":"closePO", "href":"http://localhost:9000/rest/pos/10001", "method":"DELETE"},
                    { "rel":"requestPOUpdate", "href":"http://localhost:9000/rest/pos/10001/updates","method":"POST" }
                ]
            },
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }
        
### Reject a Purchase Order[DELETE]
+ Response 200 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[
                    { "rel":"updatePO", "href":"http://localhost:9000/rest/pos/10001", "method":"PUT"},
                ]
            },
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }


## Handle existing  Purchase Order [/rest/pos/10001]
### Update Rejected Purchase Order [PUT]
+ Request (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[]
            },
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }
+ Response 200 (application/json)   

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[
                ]
            },
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }
+ Response 409 (application/json)   
 
        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[
                { "rel":"updatePO", "href":"http://localhost:9000/rest/pos/10001", "method":"PUT" }
                ]
            },
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }

### Close Purchse Order[DELETE]
+ Response 200 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[
                { "rel":"updatePO", "href":"http://localhost:9000/rest/pos/10001", "method":"PUT" }
                ]
            },
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }
        
## Request Update to Purchase Order [/rest/pos/10001/updates]
### Request Update to Purchase Order [PUT]
+ Request (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[
                ]
            },
            "startDate":"2015-11-15",
            "endDate":"2015-11-16"
        }
+ Response 200 (application/json)   

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[
                    { "rel":"rejectPOUpdate","href":"http://localhost:9000/rest/pos/10001/updates/10001/accept","method":"DELETE" },
                    { "rel":"acceptPOUpdate","href":"http://localhost:9000/rest/pos/10001/updates/10001/accept","method":"POST" }
                ]
            },
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }
+ Response 409 (application/json)   

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[
                { "rel":"updatePO", "href":"http://localhost:9000/rest/pos/10001", "method":"PUT" }
                ]
            },
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }
+ Request (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[]
            },
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }
+ Response 200 (application/json)   

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[
                ]
            },
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }
+ Response 409 (application/json)   

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[
                { "rel":"updatePO", "href":"http://localhost:9000/rest/pos/10001", "method":"PUT" }
                ]
            },
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }


## Handle Update Requests [/rest/pos/10001/updates/10001/accept]
### Accept an Update Request[POST]
+ Response 200 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ],
                "_links":[
                    { "rel":"closePO", "href":"http://localhost:9000/rest/pos/10001", "method":"DELETE"},
                    { "rel":"requestPOUpdate", "href":"http://localhost:9000/rest/pos/10001/updates","method":"POST" }
                ]
            },
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }
        
### Reject an Update Request[DELETE]
+ Response 209 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost:9000/rest/pos/10001"}
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ]
                },
                "_links":[
                    { "rel":"updatePO", "href":"http://localhost:9000/rest/pos/10001", "method":"PUT"},
                ]
            ,
            "startDate":"2015-11-12",
            "endDate":"2015-11-14"
        }


        
# Group Plants
Notes related resources of the **Plants API**

## Retrieve Plant [/rest/plants]
### Retrieve Plant Catalog [GET] 
+ Response 200 (application/json)

        [{
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" },
                {"rel":"createPHR","method"="POST","href":"http://localhost:9000/rest/phrs/form"}
            ],
            "name":"Truck",
            "description":"Awesome",
            "price":7.1
        },
        {
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                {"rel":"createPHR","method"="POST","href":"http://localhost:9000/rest/phrs/form"}

            ],
            "name":"Truck",
            "description":"Awesome",
            "price":3.1
        }
        ]

### Retrieve Plant [/plant/find]
## by name and availability [GET]
+ Request (application/json)

        {
            "name":"Truck",
            "startDate":"2015-11-12"
            "endDate":"2015-11-12"
    

        }
+ Response 200 (application/json)

        [{
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" },
            ],
            "_links":[
                {"rel":"createPHR","method"="POST","href":"http://localhost:9000/rest/phrs/form"}
            ],
            "name":"Truck",
            "description":"Awesome",
            "price":7.1
        },
        {
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
            ],
            "_links":[
                {"rel":"createPHR","method"="POST","href":"http://localhost:9000/rest/phr/form"}

            ],            
            "name":"Truck",
            "description":"Awesome",
            "price":3.1
        }
        ]

        
# Group Plant Hire Requests
Notes related resources of the **Plants API**

## Retrieve Plant Hire Request [/rest/phrs]
### Retrieve All Plant Hire Requests [GET] 
+ Response 200 (application/json)

        [
        {
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/phr/10001" },
            ],
            "_links":[
            
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ]
            "purchaseOrder":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/pos/10001" }
                ]    
            "name":"Truck",
            "description":"Awesome",
            "price":7.1
        }.
        {
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/phr/10002" },
            ],
            "_links":[
            
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10002" }
                ]
            "purchaseOrder":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/pos/10002" }
                ]
            "name":"Truck",
            "description":"Awesome bad",
            "price":3.1
        }        
        ]

## Approve Plant Hire Request [/rest/phrs/10001 ]
### Approve All Plant Hire Requests [POST] 
+ Request (application/Json)

        {
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/phr/10001" },
            ],
            "_links":[
            
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ]
            "name":"Truck",
            "description":"Awesome",
            "price":7.1
        }.
        
+ Response 200 (application/json)
        
        {
        {
            "links":[
                { "rel":"self", "href":"http://localhost:9000/rest/phr/10001" },
            ],
            "_links":[
            
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/plants/10001" }
                ]
            "purchaseOrder":{
                "links":[
                    { "rel":"self", "href":"http://localhost:9000/rest/pos/10001" }
                ]    
            "name":"Truck",
            "description":"Awesome",
            "price":7.1
        }.
         
        }

        
        



